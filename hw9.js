var studentsAndPoints = [
    'Алексей Петров', 0,
    'Ирина Овчинникова', 60,
    'Глеб Стукалов', 30,
    'Антон Павлович', 30,
    'Виктория Заровская', 30,
    'Алексей Левенец', 70,
    'Тимур Вамуш', 30,
    'Евгений Прочан', 60,
    'Александр Малов', 0,
];

//1. Создать конструктор Student для объекта студента, который принимает имя и кол-во баллов в качестве аргумента. Все объекты созданные этим конструктором должны иметь общий метод show, который работает так же как в предыдущем домашнем задании.

function Student(name, points) {
    this.name = name;
    this.points = points;
}

Student.prototype.show = function() {
    console.log('Студент %s набрал %d баллов', this.name, this.points);
}

//2. Создать конструктор StudentList, который в качестве аргумента принимает название группы и массив формата studentsAndPoints из задания по массивам. Массив может отсутствовать или быть пустым. Объект, созданный этим конструктором должен обладать всеми функциями массива. А так же иметь метод add, который принимает в качестве аргументов имя и кол-во баллов, создает нового студента и добавляет его в список.

function StudentList(groupName, students) {
    this.groupName = groupName;
    if (arguments.length > 1) {
        for (var i = 0, imax = students.length; i < imax; i += 2) {
            this.add(students[i], students[i + 1]);
        }
    }
}

StudentList.prototype = Object.create(Array.prototype);

StudentList.prototype.add = function(name, points) {
    this.push(new Student(name, points));
};

//3. Создать список студентов группы «HJ-2» и сохранить его в переменной hj2. Воспользуйтесь массивом studentsAndPoints из файла.

var hj2 = new StudentList('HJ-2', studentsAndPoints);

//4. Добавить несколько новых студентов в группу. Имена и баллы придумайте самостоятельно.

hj2.add('Иван Простоквашин', 0);
hj2.add('Василиса Тихоомутова', 0);
hj2.add('Анастасия Балалайкина', 10);

//5. Создать группу «HTML-7» без студентов и сохранить его в переменной html7. После чего добавить в группу несколько студентов. Имена и баллы придумайте самостоятельно.

var html7 = new StudentList('HTML-7');

html7.add('Петр Закулисов', 0);
html7.add('Наталья Терентьева', 10);
html7.add('Кирилл Васильев', 30);
html7.add('Анна Перепелкина', 0);

//6. Добавить спискам студентов метод show, который выводит информацию о группe. Вывести информацию о группах «HJ-2» и «HTML-7». 

StudentList.prototype.show = function () {
    console.group('Группа %s (%d студентов):', this.groupName, this.length);
    this.forEach(function (student) {
        student.show();
    });
    console.groupEnd();
}

hj2.show();
html7.show();

//7. Перевести одного из студентов из группы «HJ-2» в группу «HTML-7»

StudentList.prototype.move = function (studentName, anotherGroup) {
    var i = this.findIndex(function (elem) {
      return elem.name == studentName;
    });
    var elem = this.splice(i, 1);
    anotherGroup.push(elem[0]);
}

hj2.move('Иван Простоквашин', html7);
hj2.move('Виктория Заровская', html7);

hj2.show();
html7.show();

//8. Добавить спискам студентов метод max, который возвращает студента с наибольшим баллом в этой группе. Изучите документацию метода valueOf в Object.prototype, и попробуйте использовать Math.max для решения этой задачи.

Student.prototype.valueOf = function () {
    return this.points;
}

StudentList.prototype.max = function () {
    var max = Math.max.apply(null, this); //поняла о чем вы: можно делать поиск по самому массиву, а не создавать новый массив и искать по нему. 
    var student = this.find(function (elem, i) {
        if (elem.points == max) return elem;
    });
  
    return student;
}

console.log(hj2.max());
console.log(html7.max());